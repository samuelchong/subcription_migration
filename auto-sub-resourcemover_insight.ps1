<#
.SY NOPSIS
   Migrate resources from one subscription to another. 

.DESCRIPTION
    Use for EA migration. 

.PARAMETER Source Subscription
    Source subscription. aka old subscription - e.g. '8e328049-1bc4-4c8c-8d1f-8fs2'. 

.PARAMETER Destination Subscription 
    Destination subscription. aka new subscription - e.g. '66628049-1bc4-4c8c-8d1f-8fs2'. 

.PARAMETER Tenant ID
   Tenant ID - e.g. '111c6c70-30c8-49a7-bf21-32d635c6f5072'. 

.PARAMETER Dry Run
    Specify this flag to run in dry-run mode; When speicified, the script will skip performing the actual resource move

.EXAMPLE
    C:\PS> .\auto-sub-resourcemover_insight.ps1
    C:\PS> .\auto-sub-resourcemover_insight.ps1 -Dryrun
    C:\PS> .\auto-sub-resourcemover_insight.ps1 -s "6ecd9a5e-cd8a-4317-9987-03fdfaba2xxx" -d "8e328049-1bc4-4c8c-8d1f-8f61d3fdxxx" -t 172c6c70-30c8-49a7-bf31-32d635c6fxxx
 #>

Param(
  [Alias('s')]  
  [Parameter(Mandatory=$true)] [string]$SourceSubscription,
  [Alias('d')]
  [Parameter(Mandatory=$true)] [string]$DestinationSubscription,
  [Alias('t')]
  [Parameter(Mandatory=$true)] [string]$TenantId,
  [Alias('dry')]
  [switch]$DryRun
)

##Change the Source subscription ID and the Target subscription ID here and save this script under the subscription named folder##
$sourcesub = $SourceSubscription
$targetsub = $DestinationSubscription
$tenantId = $TenantId
$dryrun = $DryRun

$Invocation = (Get-Variable MyInvocation -Scope 0).Value
$scriptPath = Split-Path $Invocation.MyCommand.Path
$buildDir = Split-Path $scriptPath
$deployDate = Get-Date -format "yyyyMMdd-HHmmss"

Start-Transcript -path "$buildDir\logs\EAMigration\$sourcesub\log-$deployDate.txt" -NoClobber
Write-Host "Source Subscription (OLD): $SourceSubscription"
Write-Host "Destination Subscription (NEW): $targetsub"
Write-Host "Tenant ID: $tenantId"
Write-Host "Dry-Run: $DryRun"

######################
# Prerequisites
######################

# Az module are required to be installed
# Install-Module -Name Az -Scope CurrentUser -Repository PSGallery -Force

## Ensure boot diagnostic is set to 'managed'

## Manually remove Azure Updates and stop any Azure VM Backups
#https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/move-limitations/virtual-machines-move-limitations

## Unsupported resources
# Note: these resources need to be moved manually or redeployed. 
# Resource Groups containing the following will not be moved automatically.
# Manual Check for Standard SKU Public IP / Load Balancers Required. 
# https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/move-support-resources




#############################
# Login 
#############################
az login
Connect-AzAccount -TenantId $tenantId
Set-AzContext -SubscriptionId $sourcesub #old-subscription

$x = Get-AzContext
if (!$x)
{
    Throw "Run Add-AzAccount to login"
}

<##====================================================================================
Confirming source and destination subscription
##===================================================================================#>
$OldSubName = Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Name
Write-Host '------------------------------------------------------------------------------------------'
write-host 'Source Subscription -' $OldSubName '-' $sourcesub -ForegroundColor Green
Write-Host '------------------------------------------------------------------------------------------'
$NewSubName = Get-AzSubscription -SubscriptionId $targetsub | Select-Object -ExpandProperty Name
Write-Host '------------------------------------------------------------------------------------------'
Write-Host 'Target Subscription -' $NewSubName '-' $targetsub -ForegroundColor Magenta
Write-Host '------------------------------------------------------------------------------------------'

Read-Host -Prompt "Confirm moving resources from subscription '$OldSubName' to  '$NewSubName'? Press any key to CONTINUE or CTRL+C to quit"


<##====================================================================================
Cache Azure Credentials - Review Subscription Quotas
##===================================================================================#>

Write-Host "Checking Subscription Quota Limits" -ForegroundColor Magenta
$DeployedLocations = @('Australia East', `
'Australia SouthEast')

foreach($DeployedLocation in $DeployedLocations) {
    Write-Host '------------------------------'
    Write-Host $DeployedLocation
    Write-Host '------------------------------'
  Get-AzNetworkUsage `
  -Location $DeployedLocation `
  | Where-Object {$_.CurrentValue -gt 0} `
  | Format-Table ResourceType, CurrentValue, Limit
 
  Get-AzStorageUsage `
  -Location $DeployedLocation `
  | Where-Object {$_.CurrentValue -gt 0}
 
  Get-AzVMUsage `
  -Location $DeployedLocation `
  | Where-Object {$_.CurrentValue -gt 0}
}

Write-Host "Check Quotas Completed - Registering missing resource providers" -ForegroundColor Magenta
<##====================================================================================
Register resource providers in the new subscription to match the source subscription
##===================================================================================#>


Set-AzContext -SubscriptionId $sourcesub #old-subscription
$ExistingResourceProvidersInOldSubscription = Get-AzResourceProvider


Set-AzContext -SubscriptionId $targetsub #new-subscription
$ExistingResourceProvidersInNewSubscription = Get-AzResourceProvider


foreach($ExistingResourceProviderInOldSubscription in $ExistingResourceProvidersInOldSubscription) {
    if($ExistingResourceProviderInOldSubscription.RegistrationState -eq 'Registered'){
        $providerfound = $false;
        foreach($ExistingResourceProviderInNewSubscription in $ExistingResourceProvidersInNewSubscription){
                if($ExistingResourceProviderInNewSubscription.ProviderNamespace -eq $ExistingResourceProviderInOldSubscription.ProviderNamespace){
                    $providerfound = $true;
                    if($ExistingResourceProviderInNewSubscription.RegistrationState -eq 'Registered'){
                        Write-Host '------------------------------'
                        Write-Host $ExistingResourceProviderInOldSubscription.ProviderNamespace 'PRESENT, REGISTERED'
                        Write-Host '------------------------------'
                    }
                    else{
                        Write-Host $ExistingResourceProviderInOldSubscription.ProviderNamespace 'PRESENT, UNREGISTERED, Registering'
                        Register-AzResourceProvider -ProviderNamespace $ExistingResourceProviderInOldSubscription.ProviderNamespace -WhatIf:$false
                    }                        
                }
            }
        if(-Not($providerfound)){
            Write-Host $ExistingResourceProviderInOldSubscription.ProviderNamespace 'ABSENT, Registering'
            Register-AzResourceProvider -ProviderNamespace $ExistingResourceProviderInOldSubscription.ProviderNamespace -WhatIf:$false
        }
    }
}

Write-Host "Waiting for registrations to complete" -ForegroundColor Magenta
While ((Get-AzResourceProvider -ListAvailable | Where-Object {$_.registrationstate -ne "Registered" -and $_.registrationstate -ne "NotRegistered"}).count -gt 0)

{

    Start-Sleep -seconds 5

}

Write-Host "Resource Providers Registration Completed - Next Stage Gathering Deployed Resource Details" -ForegroundColor Magenta
<##====================================================================================
Output moveable resources into CSV - Ensuring all types included in the move collection are set to True
##===================================================================================#>

Write-Host "Set AzContext to source subscription" -ForegroundColor Magenta
Set-AzContext -SubscriptionId $sourcesub #old-subscription

#A Class to hold information to be used for discussion later
class Resource{
    [string]$ResourceGroupName
    [string]$Name
    [string]$Moveable
    [string]$Owner
    [string]$Comments;

    Resource(
    [string]$ResourceGroupName,
    [string]$Name,
    [string]$Moveable,
    [string]$Owner,
    [string]$Comments
    ){
        $this.ResourceGroupName = $ResourceGroupName
        $this.Name = $Name
        $this.Moveable = $Moveable
        $this.Owner = $Owner
        $this.Comments = $Comments
    }
}


$ResultObject = @([Resource]::new('--', '--', '--', '--', '--'))

# Add Resource types that are not supported by Azure move tool. 
# Note: these resources need to be moved manually or redeployed. 
# Resource Groups containing the following will not be moved automatically.
# Manual Check for Standard SKU Public IP / Load Balancers Required. 
# https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/move-support-resources

$NonMovableResourceTypesWithinSameTenant = @('Microsoft.AAD/DomainServices', `
'Microsoft.ContainerService/managedClusters', `
'microsoft.insights/activityLogAlerts', `
'microsoft.visualstudio/account', `
'microsoft.classicnetwork/virtualnetworks', `
'microsoft.compute/sshpublickeys', `
'microsoft.containerinstance/containergroups', `
'microsoft.datamigration/services', `
'microsoft.datamigration/services/projects', `
'Microsoft.ManagedIdentity/userAssignedIdentities', `
'microsoft.network/applicationgateways', `
'microsoft.network/natgateways', `
'Sendgrid.Email/accounts')

###########################################
#Public IP Standard SKU check Automation
###########################################
Write-Host "Checking Public IP SKU and output to IP-Record.csv" -ForegroundColor Magenta
$PipSkuType = get-azresource -resourcetype "microsoft.network/publicipaddresses" | Where-Object {$_.ResourceGroupName -ne 'MC_boa-sandpit-shared01-core_insight-dkr_australiaeast'}
foreach ($PipSku in $PipSkuType){
    $PipSku.name
    $PipSku.ResourceGroupName

    $IPAddresses = (Get-AzPublicIpAddress -Name $PipSku.name -ResourceGroupName $PipSku.ResourceGroupName).IpAddress
    $IPSkuName = (Get-AzPublicIpAddress -Name $PipSku.name -ResourceGroupName $PipSku.ResourceGroupName).Sku.Name
    $IPOutput = $IPAddresses + "," + $PipSku.name + "," + $PipSku.ResourceGroupName + "," + $IPSkuName
    Write-Host '------------------------------'
    Write-Host $IPOutput -ForegroundColor Green
    Write-Host '------------------------------'
    $IPOutput | Add-Content $buildDir\logs\EAMigration\$sourcesub\IP-Record.csv
    
if ($IPSkuName -ne "Basic") {$NonMovableResourceTypesWithinSameTenant += 'microsoft.network/publicipaddresses'}      
}

###############################
# Resource Type Exclusion List
###############################

Write-Host "Add resource and resource group to exclusion list" -ForegroundColor Magenta
# Add Resource types that dont need to be migrated to this list
$ExcludedResources = @('microsoft.compute/restorepointcollections', `
'microsoft.insights/metricalerts', `
'microsoft.network/networkwatchers')
# Add Resource Groups that dont need to be migrated to this list
$ExcludedResourceGroups = @('NetworkWatcherRG', `
'DefaultResourceGroup-ASE', `
'DefaultResourceGroup-EAU', `
'defaultresourcegroup-eau', `
'Default-EventGrid', `
'AzureBackupRG_australiaeast_1')

$ExcludeList = $NonMovableResourceTypesWithinSameTenant += $ExcludedResources


##################################
# Get all resources for migration
##################################
$StaticRG = Get-AzResourceGroup | Where-Object {$ExcludedResourceGroups -notcontains $_.ResourceGroupName} 

foreach ($RG in $StaticRG){
    Write-Host $RG.ResourceGroupName
    $AllResources = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | Where-Object {$ExcludedResources -notcontains $_.ResourceType} 

    $Name = ''
    $Moveable = ''
    $Owner = ''#$Rg.Tags['Owner']
    $Comments = ''
    foreach ($Resource in $AllResources){
        $ResourceType = $Resource.ResourceType
        if($NonMovableResourceTypesWithinSameTenant -contains $Resource.ResourceType){
            $Moveable = 'False'
            $Comments = "Move operation not supported on this Resource Group because --  $ResourceType -- does not support move"
        }
        else{
            $Moveable = 'True'
            $Comments = "Move operation supported for: $ResourceType"
        }
        
        $ResultObject += [Resource]::new($RG.ResourceGroupName, $Resource.Name, $Moveable, $Owner, $Comments)
    }
}

Write-Host "Output moveable resources into CSV" -ForegroundColor Magenta
$ResultObject | Format-Table -AutoSize -Wrap
$ResultObject | Export-Csv -Path $buildDir\logs\EAMigration\$sourcesub\MoveableResources.csv -NoTypeInformation
$ResultObject | Where-Object { $_.Moveable -eq 'FALSE'} | Format-Table -AutoSize -Wrap
$ResultObject | Group-Object -Property ResourceGroupName
$StaticRG | Group-Object -Property ResourceGroupName

####################################
# Exit here if dry run is specified
####################################
if ($DryRun -eq $True)
{
  Write-Host "DryRun flag is specified. Script will terminate here without proceeding to actual resource move. Refer to MoveableResources.csv"   
  exit 0
}

Read-Host -Prompt "Resource Groups Checked - Confirm output MoveableResources.csv - Press any key to Prepare VMs for moves or CTRL+C to quit"


<##====================================================================================
Virtual machines move prep 
##===================================================================================#>

#Manually remove Azure Updates and stop any Azure VM Backups
#https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/move-limitations/virtual-machines-move-limitations

Set-AzContext -SubscriptionId $sourcesub #old-subscription
Write-Host "Checking VM SKUs - Disabling Disk Encryption - Removing" -ForegroundColor Magenta
foreach ($RG in $StaticRG){
    $AllVMS = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | where-object -eq ResourceType "Microsoft.Compute/Virtualmachines"

    foreach ($VM in $AllVMS){

        write-host $VM.Name
        Write-Host $RG.ResourceGroupName
        Disable-AzVMDiskEncryption -ResourceGroupName $RG.ResourceGroupName -VMName $VM.Name -Force
        Get-AzVmDiskEncryptionStatus -VMName $VM.Name -ResourceGroupName $RG.ResourceGroupName
        
        $info = ((get-azvm  -ResourceGroupName $RG.ResourceGroupName -Name $VM.Name).storageprofile).imagereference
        write-host $info.offer
        write-host $info.sku
    }
}

#####################################
# Remove restore point collection
#####################################

Write-Output "Set AzContext to use source subscription."
Set-AzContext -SubscriptionId $sourcesub #old-subscription
##Add Restore Point Collections for deletion##
$RestorePointRGs = @('AzureBackupRG_australiasoutheast_1', `
'AzureBackupRG_australiaeast_1')

# $restorePointCollection = Get-AzResource -ResourceGroupName $RestorePointRGs -ResourceType Microsoft.Compute/restorePointCollections
$restorePointCollection = $RestorePointRGs | ForEach-Object {
    if (Get-AzResourceGroup -ResourceGroupName $_ -ErrorAction SilentlyContinue)
    {
        Get-AzResource -ResourceGroupName $_ -ResourceType Microsoft.Compute/restorePointCollections
    }
}
Write-Output "Remove restore point collections"
foreach ($restorePoint in $restorePointCollection){
  Remove-AzResource -ResourceId $restorePoint.ResourceId -Force
}

Read-Host -Prompt "VMs Validation Complete - Press any key to create Resource Groups"
<##====================================================================================
Create Resource Groups in the target subscription. Will not create Resource Groups that have been excluded
##===================================================================================#>
Write-Output "Create Resource Groups in the target subscription. Will not create Resource Groups that have been excluded"
Write-Output "Set AzContext to use target subscription."
Set-AzContext -SubscriptionId $targetsub #new-subscription

foreach ($RG in $StaticRG){
    Write-Host $RG.ResourceGroupName
    New-AzResourceGroup -Name $RG.ResourceGroupName -Location $RG.Location -Tag $RG.Tags -WhatIf:$false -Confirm:$false
}

Read-Host -Prompt "Resource Groups Created - Check Azure Portal - Press any key to move resources once ready or CTRL+C to quit"
<##====================================================================================
Resources to be moved from the filtered list
##===================================================================================#>

#$RGsToBeMoved = @('MyRG') 

Set-AzContext -SubscriptionId $sourcesub #old-subscription

write-host "Moving resource from source to target subscription" -ForegroundColor Magenta
foreach($RG in $StaticRG){
    #if($RGsToBeMoved -contains $RG.ResourceGroupName){
    $ResourceIdArray = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | Where-Object {!($_.ParentResource) -and ($ExcludeList -notcontains $_.ResourceType)} | ForEach-Object {"$($_.ResourceId)"} 
    write-host $ResourceIdArray
    Move-AzResource -DestinationResourceGroupName $RG.ResourceGroupName -DestinationSubscriptionId $targetsub -ResourceId $ResourceIdArray `
        -WhatIf:$false -Confirm:$false -Verbose -Debug -Force
    #}
}

Read-Host -Prompt "Resource Move Completed. Press any key to run Clean Up Tasks or CTRL+C to quit"
<##====================================================================================
Rename Old Sub & Move Management Group - Copy Tags - Re-encrypt VM disks - Update Automation Account IDs
##===================================================================================#>

write-host "Renaming old Subscription and moving Subscription to the decom management group" -ForegroundColor Magenta

$OldSubName = Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Name
$RenameSub = $OldSubName + "-decom"

##Rename old Subscription and move to decom management group
Write-Host "Rename old Subscription and move to decom management group" -ForegroundColor Magenta
az account subscription rename --subscription-id $sourcesub --name $RenameSub
az account management-group subscription add --name "sfts-decom-mg" --subscription $sourcesub

$OldSubName = Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Name
Write-Host '------------------------------'
write-host $OldSubName -ForegroundColor DarkGreen
Write-Host '------------------------------'

Set-AzContext -SubscriptionId $targetsub #new-subscription

##Automation Accounts Update Sub ID
write-host "Updating Automation Accounts Subscription ID" -ForegroundColor Magenta
$AutomationAccounts = get-azresource -resourcetype Microsoft.Automation/automationaccounts

foreach ($Account in $AutomationAccounts){
    $AutomationName = get-azresource -resourcetype Microsoft.Automation/automationaccounts | Select-Object -ExpandProperty Name
    $AutomationRG = get-azresource -resourcetype Microsoft.Automation/automationaccounts | Select-Object -ExpandProperty ResourceGroupName
    Get-AzureAutomationConnection -AutomationAccountName $AutomationName -Name "AzureRunAsConnection"
    Set-AzAutomationConnectionFieldValue -Name "AzureRunAsConnection" -ConnectionFieldName "SubscriptionId" -Value $targetsub -ResourceGroupName $AutomationRG -AutomationAccountName $AutomationName
}

##Copy Tags to Target Sub
write-host "Copying Tags to the new subscription"
Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Tags | ConvertTo-Json | ConvertFrom-Json | Export-Csv $buildDir\logs\EAMigration\$sourcesub\Tags.csv -NoTypeInformation

$tags = Import-Csv $buildDir\logs\EAMigration\$sourcesub\Tags.csv

foreach($head in $tags.psobject.Properties.Name){
   $newtag = @{$head = $tags.$head}
   Update-AzTag -ResourceID /subscriptions/$targetsub -Tag $newtag -Operation Merge -Verbose
}

##Re-encrypt VM disks
write-host "Re-encrypting Azure VM disks"
Set-AzContext -SubscriptionId $targetsub #new-subscription

$NewRG = Get-AzResourceGroup | Where-Object {$ExcludedResourceGroups -notcontains $_.ResourceGroupName} 
foreach ($RG in $NewRG){
    $AllVMS = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | where-object -eq ResourceType "Microsoft.Compute/Virtualmachines"

    foreach ($VM in $AllVMS){

        write-host $VM.Name
        Write-Host $RG.ResourceGroupName
        $KeyVaultName = Get-AzKeyVault -ResourceGroupName $RG.ResourceGroupName | Select-Object -ExpandProperty "vaultName"
        Write-Host '------------------------------'
        Write-Host $KeyVaultName -ForegroundColor DarkCyan
        Write-Host '------------------------------'

        $KeyVault = Get-AzKeyVault -VaultName $KeyVaultName -ResourceGroupName $RG.ResourceGroupName
        Set-AzVMDiskEncryptionExtension -ResourceGroupName $RG.ResourceGroupName -VMName $VM.Name -DiskEncryptionKeyVaultUrl $KeyVault.VaultUri -DiskEncryptionKeyVaultId $KeyVault.ResourceId -force
        
        $encyptStatus = Get-AzVmDiskEncryptionStatus -VMName $VM.Name -ResourceGroupName $RG.ResourceGroupName
        Write-Host '------------------------------'
        Write-Host $encyptStatus -ForegroundColor DarkCyan
        Write-Host '------------------------------'
        
    }
}

Read-Host -Prompt "Resource Move Completed. Do you want to create resource locks - Press any key to lock resources or CTRL+C to quit"
<##====================================================================================
Set Delete Lock on Resource Groups in new subscription
##===================================================================================#>

Set-AzContext -SubscriptionId $targetsub #new-subscription

foreach ($resourcegroup in $StaticRG){
    $resourcegroupname = $resourcegroup.ResourceGroupName
    Write-Host "RG Name $resourcegroupname"
    if(Get-AzResourceLock -ResourceGroupName $resourcegroupname -AtScope){
        Write-Host "Lock Present Already"
        Set-AzResourceLock -LockName "Do-Not-Delete" -LockLevel CanNotDelete -ResourceGroupName $resourcegroupname -LockNotes 'tssinfrastructureteam@steadfasttech.com.au'
        Get-AzResourceLock -ResourceGroupName $resourcegroupname -AtScope | Format-Table -AutoSize -Wrap
    }
    else{
        Write-Host "Setting the lock"
        Set-AzResourceLock -LockName "Do-Not-Delete" -LockLevel CanNotDelete -ResourceGroupName $resourcegroupname -LockNotes 'tssinfrastructureteam@steadfasttech.com.au'
        #Set-AzResourceLock -LockName "Read-Only" -LockLevel ReadOnly -ResourceGroupName $resourcegroupname
    }
}
Stop-Transcript