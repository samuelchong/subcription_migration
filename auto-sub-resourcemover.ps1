##Change the Source subscription ID and the Target subscription ID here and save this script under the subscription named folder##

$sourcesub = "xxxx-xxxx-xxxx-xxxx-xxxxx"  #OLD-Subscription-ID
$targetsub = "xxxx-xxxx-xxxx-xxxx-xxxxx"  #NEW-Subscription-ID

<##====================================================================================
Cache Azure Credentials - Review Subscription Quotas
##===================================================================================#>

az login
Connect-AzAccount -TenantId c887a8ab-6a75-4cf6-a109-04fd65c30162
Set-AzContext -SubscriptionId $sourcesub #old-subscription

$OldSubName = Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Name
Write-Host '------------------------------------------------------------------------------------------'
write-host 'Source Subscription -' $OldSubName '-' $sourcesub -ForegroundColor Green
Write-Host '------------------------------------------------------------------------------------------'
$NewSubName = Get-AzSubscription -SubscriptionId $targetsub | Select-Object -ExpandProperty Name
Write-Host '------------------------------------------------------------------------------------------'
Write-Host 'Target Subscription -' $NewSubName '-' $targetsub -ForegroundColor Magenta
Write-Host '------------------------------------------------------------------------------------------'

Write-Host "Checking Subscription Quota Limits"
$DeployedLocations = @('Australia East', `
'Australia SouthEast')

foreach($DeployedLocation in $DeployedLocations) {
    Write-Host '------------------------------'
    Write-Host $DeployedLocation
    Write-Host '------------------------------'
  Get-AzNetworkUsage `
  -Location $DeployedLocation `
  | Where-Object {$_.CurrentValue -gt 0} `
  | Format-Table ResourceType, CurrentValue, Limit
 
  Get-AzStorageUsage `
  -Location $DeployedLocation `
  | Where-Object {$_.CurrentValue -gt 0}
 
  Get-AzVMUsage `
  -Location $DeployedLocation `
  | Where-Object {$_.CurrentValue -gt 0}
}

Write-Host "Check Quotas Completed - Registering missing resource providers"
<##====================================================================================
Register resource providers in the new subscription to match the source subscription
##===================================================================================#>


Set-AzContext -SubscriptionId $sourcesub #old-subscription
$ExistingResourceProvidersInOldSubscription = Get-AzResourceProvider


Set-AzContext -SubscriptionId $targetsub #new-subscription
$ExistingResourceProvidersInNewSubscription = Get-AzResourceProvider


foreach($ExistingResourceProviderInOldSubscription in $ExistingResourceProvidersInOldSubscription) {
    if($ExistingResourceProviderInOldSubscription.RegistrationState -eq 'Registered'){
        $providerfound = $false;
        foreach($ExistingResourceProviderInNewSubscription in $ExistingResourceProvidersInNewSubscription){
                if($ExistingResourceProviderInNewSubscription.ProviderNamespace -eq $ExistingResourceProviderInOldSubscription.ProviderNamespace){
                    $providerfound = $true;
                    if($ExistingResourceProviderInNewSubscription.RegistrationState -eq 'Registered'){
                        Write-Host '------------------------------'
                        Write-Host $ExistingResourceProviderInOldSubscription.ProviderNamespace 'PRESENT, REGISTERED'
                        Write-Host '------------------------------'
                    }
                    else{
                        Write-Host $ExistingResourceProviderInOldSubscription.ProviderNamespace 'PRESENT, UNREGISTERED, Registering'
                        Register-AzResourceProvider -ProviderNamespace $ExistingResourceProviderInOldSubscription.ProviderNamespace -WhatIf:$false
                    }                        
                }
            }
        if(-Not($providerfound)){
            Write-Host $ExistingResourceProviderInOldSubscription.ProviderNamespace 'ABSENT, Registering'
            Register-AzResourceProvider -ProviderNamespace $ExistingResourceProviderInOldSubscription.ProviderNamespace -WhatIf:$false
        }
    }
}

Write-Host "Waiting for registrations to complete"
While ((Get-AzResourceProvider -ListAvailable | Where-Object {$_.registrationstate -ne "Registered" -and $_.registrationstate -ne "NotRegistered"}).count -gt 0)

{

    Start-Sleep -seconds 5

}

Write-Host "Resource Providers Registration Completed - Next Stage Gathering Deployed Resource Details"
<##====================================================================================
Output moveable resources into CSV - Ensuring all types included in the move collection are set to True
##===================================================================================#>

Set-AzContext -SubscriptionId $sourcesub #old-subscription

#A Class to hold information to be used for discussion later
class Resource{
    [string]$ResourceGroupName
    [string]$Name
    [string]$Moveable
    [string]$Owner
    [string]$Comments;

    Resource(
    [string]$ResourceGroupName,
    [string]$Name,
    [string]$Moveable,
    [string]$Owner,
    [string]$Comments
    ){
        $this.ResourceGroupName = $ResourceGroupName
        $this.Name = $Name
        $this.Moveable = $Moveable
        $this.Owner = $Owner
        $this.Comments = $Comments
    }
}


$ResultObject = @([Resource]::new('--', '--', '--', '--', '--'))

# Add Resource types that are not supported by Azure move tool. 
# Note: these resources need to be moved manually or redeployed. 
# Resource Groups containing the following will not be moved automatically.
# Manual Check for Standard SKU Public IP / Load Balancers Required. 
# https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/move-support-resources

$NonMovableResourceTypesWithinSameTenant = @('Microsoft.AAD/DomainServices', `
'Microsoft.ContainerService/managedClusters', `
'microsoft.insights/activityLogAlerts', `
'microsoft.visualstudio/account', `
'microsoft.classicnetwork/virtualnetworks', `
'microsoft.compute/sshpublickeys', `
'microsoft.containerinstance/containergroups', `
'microsoft.datamigration/services', `
'microsoft.datamigration/services/projects', `
'Microsoft.ManagedIdentity/userAssignedIdentities', `
'microsoft.network/applicationgateways', `
'microsoft.network/natgateways', `
'Sendgrid.Email/accounts')

#Public IP Standard SKU check Automation
$PipSkuType = get-azresource -resourcetype "microsoft.network/publicipaddresses" | Where-Object {$_.ResourceGroupName -ne 'MC_boa-sandpit-shared01-core_insight-dkr_australiaeast'}
foreach ($PipSku in $PipSkuType){
    $PipSku.name
    $PipSku.ResourceGroupName

    $IPAddresses = (Get-AzPublicIpAddress -Name $PipSku.name -ResourceGroupName $PipSku.ResourceGroupName).IpAddress
    $IPSkuName = (Get-AzPublicIpAddress -Name $PipSku.name -ResourceGroupName $PipSku.ResourceGroupName).Sku.Name
    $IPOutput = $IPAddresses + "," + $PipSku.name + "," + $PipSku.ResourceGroupName + "," + $IPSkuName
    Write-Host '------------------------------'
    Write-Host $IPOutput -ForegroundColor Green
    Write-Host '------------------------------'
    $IPOutput | Add-Content .\IP-Record.csv
    
if ($IPSkuName -ne "Basic") {$NonMovableResourceTypesWithinSameTenant += 'microsoft.network/publicipaddresses'}      
}

# Add Resource types that dont need to be migrated to this list
$ExcludedResources = @('microsoft.compute/restorepointcollections', `
'microsoft.insights/metricalerts', `
'microsoft.network/networkwatchers')
# Add Resource Groups that dont need to be migrated to this list
$ExcludedResourceGroups = @('NetworkWatcherRG', `
'DefaultResourceGroup-ASE', `
'DefaultResourceGroup-EAU', `
'defaultresourcegroup-eau', `
'Default-EventGrid', `
'AzureBackupRG_australiaeast_1')

$ExcludeList = $NonMovableResourceTypesWithinSameTenant += $ExcludedResources

$StaticRG = Get-AzResourceGroup | Where-Object {$ExcludedResourceGroups -notcontains $_.ResourceGroupName} 

foreach ($RG in $StaticRG){
    Write-Host $RG.ResourceGroupName
    $AllResources = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | Where-Object {$ExcludedResources -notcontains $_.ResourceType} 

    $Name = ''
    $Moveable = ''
    $Owner = ''#$Rg.Tags['Owner']
    $Comments = ''
    foreach ($Resource in $AllResources){
        $ResourceType = $Resource.ResourceType
        if($NonMovableResourceTypesWithinSameTenant -contains $Resource.ResourceType){
            $Moveable = 'False'
            $Comments = "Move operation not supported on this Resource Group because --  $ResourceType -- does not support move"
        }
        else{
            $Moveable = 'True'
            $Comments = "Move operation supported for: $ResourceType"
        }
        $ResultObject += [Resource]::new($RG.ResourceGroupName, $Resource.Name, $Moveable, $Owner, $Comments)
    }
}

$ResultObject | Format-Table -AutoSize -Wrap
$ResultObject | Export-Csv -Path .\MoveableResources.csv -NoTypeInformation
$ResultObject | Where-Object { $_.Moveable -eq 'FALSE'} | Format-Table -AutoSize -Wrap
$ResultObject | Group-Object -Property ResourceGroupName
$StaticRG | Group-Object -Property ResourceGroupName

Read-Host -Prompt "Resource Groups Checked - Confirm output MoveableResources.csv - Press any key to Prepare VMs for moves or CTRL+C to quit"
<##====================================================================================
Virtual machines move prep 
##===================================================================================#>

#Manually remove Azure Updates and stop any Azure VM Backups
#https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/move-limitations/virtual-machines-move-limitations

Set-AzContext -SubscriptionId $sourcesub #old-subscription
Write-Host "Checking VM SKUs - Disabling Disk Encryption - Removing"
foreach ($RG in $StaticRG){
    $AllVMS = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | where-object -eq ResourceType "Microsoft.Compute/Virtualmachines"

    foreach ($VM in $AllVMS){

        write-host $VM.Name
        Write-Host $RG.ResourceGroupName
        Disable-AzVMDiskEncryption -ResourceGroupName $RG.ResourceGroupName -VMName $VM.Name -Force
        Get-AzVmDiskEncryptionStatus -VMName $VM.Name -ResourceGroupName $RG.ResourceGroupName
        
        $info = ((get-azvm  -ResourceGroupName $RG.ResourceGroupName -Name $VM.Name).storageprofile).imagereference
        write-host $info.offer
        write-host $info.sku
    }
}

Set-AzContext -SubscriptionId $sourcesub #old-subscription
##Add Restore Point Collections for deletion##
$RestorePointRGs = @('AzureBackupRG_australiasoutheast_1', `
'AzureBackupRG_australiaeast_1')

$restorePointCollection = Get-AzResource -ResourceGroupName $RestorePointRGs -ResourceType Microsoft.Compute/restorePointCollections

foreach ($restorePoint in $restorePointCollection){
  Remove-AzResource -ResourceId $restorePoint.ResourceId -Force
}

Read-Host -Prompt "VMs Validation Complete - Press any key to create Resource Groups"
<##====================================================================================
Create Resource Groups in the target subscription. Will not create Resource Groups that have been excluded
##===================================================================================#>

Set-AzContext -SubscriptionId $targetsub #new-subscription

foreach ($RG in $StaticRG){
    Write-Host $RG.ResourceGroupName
    New-AzResourceGroup -Name $RG.ResourceGroupName -Location $RG.Location -Tag $RG.Tags -WhatIf:$false -Confirm:$false
}

Read-Host -Prompt "Resource Groups Created - Check Azure Portal - Press any key to move resources once ready or CTRL+C to quit"
<##====================================================================================
Resources to be moved from the filtered list
##===================================================================================#>

#$RGsToBeMoved = @('MyRG') 

Set-AzContext -SubscriptionId $sourcesub #old-subscription

foreach($RG in $StaticRG){
    #if($RGsToBeMoved -contains $RG.ResourceGroupName){
    $ResourceIdArray = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | Where-Object {!($_.ParentResource) -and ($ExcludeList -notcontains $_.ResourceType)} | ForEach-Object {"$($_.ResourceId)"} 
    write-host $ResourceIdArray
    Move-AzResource -DestinationResourceGroupName $RG.ResourceGroupName -DestinationSubscriptionId $targetsub -ResourceId $ResourceIdArray `
        -WhatIf:$false -Confirm:$false -Verbose -Debug -Force
    #}
}

Read-Host -Prompt "Resource Move Completed. Press any key to run Clean Up Tasks or CTRL+C to quit"
<##====================================================================================
Rename Old Sub & Move Management Group - Copy Tags - Re-encrypt VM disks - Update Automation Account IDs
##===================================================================================#>

write-host "Renaming old Subscription and moving Subscription to the decom management group"

$OldSubName = Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Name
$RenameSub = $OldSubName + "-decom"

##Rename old Subscription and move to decom management group

az account subscription rename --subscription-id $sourcesub --name $RenameSub
az account management-group subscription add --name "sfts-decom-mg" --subscription $sourcesub

$OldSubName = Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Name
Write-Host '------------------------------'
write-host $OldSubName -ForegroundColor DarkGreen
Write-Host '------------------------------'

Set-AzContext -SubscriptionId $targetsub #new-subscription

##Automation Accounts Update Sub ID
write-host "Updating Automation Accounts Subscription ID"
$AutomationAccounts = get-azresource -resourcetype Microsoft.Automation/automationaccounts

foreach ($Account in $AutomationAccounts){
    $AutomationName = get-azresource -resourcetype Microsoft.Automation/automationaccounts | Select-Object -ExpandProperty Name
    $AutomationRG = get-azresource -resourcetype Microsoft.Automation/automationaccounts | Select-Object -ExpandProperty ResourceGroupName
    Get-AzureAutomationConnection -AutomationAccountName $AutomationName -Name "AzureRunAsConnection"
    Set-AzAutomationConnectionFieldValue -Name "AzureRunAsConnection" -ConnectionFieldName "SubscriptionId" -Value $targetsub -ResourceGroupName $AutomationRG -AutomationAccountName $AutomationName
}

##Copy Tags to Target Sub
write-host "Copying Tags to the new subscription"
Get-AzSubscription -SubscriptionId $sourcesub | Select-Object -ExpandProperty Tags | ConvertTo-Json | ConvertFrom-Json | Export-Csv .\Tags.csv -NoTypeInformation

$tags = Import-Csv .\Tags.csv

foreach($head in $tags.psobject.Properties.Name){
   $newtag = @{$head = $tags.$head}
   Update-AzTag -ResourceID /subscriptions/$targetsub -Tag $newtag -Operation Merge -Verbose
}

##Re-encrypt VM disks
write-host "Re-encrypting Azure VM disks"
Set-AzContext -SubscriptionId $targetsub #new-subscription

$NewRG = Get-AzResourceGroup | Where-Object {$ExcludedResourceGroups -notcontains $_.ResourceGroupName} 
foreach ($RG in $NewRG){
    $AllVMS = Get-AzResource -ResourceGroupName $RG.ResourceGroupName | where-object -eq ResourceType "Microsoft.Compute/Virtualmachines"

    foreach ($VM in $AllVMS){

        write-host $VM.Name
        Write-Host $RG.ResourceGroupName
        $KeyVaultName = Get-AzKeyVault -ResourceGroupName $RG.ResourceGroupName | Select-Object -ExpandProperty "vaultName"
        Write-Host '------------------------------'
        Write-Host $KeyVaultName -ForegroundColor DarkCyan
        Write-Host '------------------------------'

        $KeyVault = Get-AzKeyVault -VaultName $KeyVaultName -ResourceGroupName $RG.ResourceGroupName
        Set-AzVMDiskEncryptionExtension -ResourceGroupName $RG.ResourceGroupName -VMName $VM.Name -DiskEncryptionKeyVaultUrl $KeyVault.VaultUri -DiskEncryptionKeyVaultId $KeyVault.ResourceId -force
        
        $encyptStatus = Get-AzVmDiskEncryptionStatus -VMName $VM.Name -ResourceGroupName $RG.ResourceGroupName
        Write-Host '------------------------------'
        Write-Host $encyptStatus -ForegroundColor DarkCyan
        Write-Host '------------------------------'
        
    }
}

Read-Host -Prompt "Resource Move Completed. Do you want to create resource locks - Press any key to lock resources or CTRL+C to quit"
<##====================================================================================
Set Delete Lock on Resource Groups in new subscription
##===================================================================================#>

Set-AzContext -SubscriptionId $targetsub #new-subscription

foreach ($resourcegroup in $StaticRG){
    $resourcegroupname = $resourcegroup.ResourceGroupName
    Write-Host "RG Name $resourcegroupname"
    if(Get-AzResourceLock -ResourceGroupName $resourcegroupname -AtScope){
        Write-Host "Lock Present Already"
        Set-AzResourceLock -LockName "Do-Not-Delete" -LockLevel CanNotDelete -ResourceGroupName $resourcegroupname -LockNotes 'tssinfrastructureteam@steadfasttech.com.au'
        Get-AzResourceLock -ResourceGroupName $resourcegroupname -AtScope | Format-Table -AutoSize -Wrap
    }
    else{
        Write-Host "Setting the lock"
        Set-AzResourceLock -LockName "Do-Not-Delete" -LockLevel CanNotDelete -ResourceGroupName $resourcegroupname -LockNotes 'tssinfrastructureteam@steadfasttech.com.au'
        #Set-AzResourceLock -LockName "Read-Only" -LockLevel ReadOnly -ResourceGroupName $resourcegroupname
    }
}
